import { readable } from 'svelte/store';

export let defaultLang = readable({
	EN: {
		sendlabel: "Send",
		receivelabel: "Receive",
		searchterm: "Country or currency code",
		tablefee: "Transfer fee",
		tablerate: "Exchange rate",
		tableservice: "Delivery service",
		cta: "Get started",
		popcor: "Popular countries",
		allcor: "All countries",
		disclaimer: "Exchange rates and fees shown are estimates and may vary by payment and payout methods or other factors and are subject to change. Click “Get started”to check current rates and other options.",
		"000": "Cash",
		"200": "Direct to Card",
		"300": "Next Day/Delayed Services",
		"500": "Direct to Bank",
		"501": "Western Union Digital Bank Account",
		"800": "Mobile Money Transfer",
	}
})
export default defaultLang;