import { readable } from 'svelte/store';

export let defaultList = readable({
	payout: [
		{ fundsout: "AG", mnemonic: "000" },
		{ fundsout: "D2C_PS", mnemonic: "200" },
		{ fundsout: "BA", mnemonic: "500" },
		{ fundsout: "ND", mnemonic: "300" },
		{ fundsout: "WU", mnemonic: "501" },
		{ fundsout: "WA", mnemonic: "800" }
	],
	payin: [
		{ fundsin: "Trustly", mnemonic: "TR" },
		{ fundsin: "TAPNGO", mnemonic: "TG" },
		{ fundsin: "SwiftIN", mnemonic: "SW" },
		{ fundsin: "Servipag", mnemonic: "SP" },
		{ fundsin: "SEPACTOUT", mnemonic: "SO" },
		{ fundsin: "SoFort", mnemonic: "SF" },
		{ fundsin: "Ideal", mnemonic: "SD" },
		{ fundsin: "SEPACTIN", mnemonic: "SC" },
		{ fundsin: "PayNow", mnemonic: "PN" },
		{ fundsin: "Poli", mnemonic: "PL" },
		{ fundsin: "PartnerFunds", mnemonic: "PF" },
		{ fundsin: "PinDebit", mnemonic: "PD" },
		{ fundsin: "Paramount", mnemonic: "PC" },
		{ fundsin: "Plaid", mnemonic: "PA" },
		{ fundsin: "MobileWallet", mnemonic: "MW" },
		{ fundsin: "SWIFTIN", mnemonic: "IT" },
		{ fundsin: "PoliPayID", mnemonic: "ID" },
		{ fundsin: "InternetBankPayment", mnemonic: "IB" },
		{ fundsin: "IACH", mnemonic: "IA" },
		{ fundsin: "GooglePay", mnemonic: "GP" },
		{ fundsin: "GoodFunds", mnemonic: "GF" },
		{ fundsin: "FPXMalaysia", mnemonic: "FP" },
		{ fundsin: "WUPay", mnemonic: "EB" },
		{ fundsin: "Dotpay", mnemonic: "DP" },
		{ fundsin: "DebitCreditCard", mnemonic: "DI" },
		{ fundsin: "DebitCard", mnemonic: "DC" },
		{ fundsin: "CreditCard", mnemonic: "CC" },
		{ fundsin: "Cash", mnemonic: "CA" },
		{ fundsin: "Bancontact", mnemonic: "BN" },
		{ fundsin: "Bancontact", mnemonic: "BC" },
		{ fundsin: "BankAccount", mnemonic: "BA" },
		{ fundsin: "ApplePay", mnemonic: "AP" },
		{ fundsin: "ACH", mnemonic: "AC" }
	  ]
})
export default defaultList;