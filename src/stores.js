import { writable } from 'svelte/store';

// Sender
export let sendIso = writable("GB");
export let sendCurnc = writable("GBP");
export let sendLang = writable("EN");

// Default Amount
export let sendAmount = writable(100);

// Receiver
export let receiveIso = writable("IN");
export let receiveCurnc = writable("INR");

// Promo (can be empty)
export let payinPromo = writable();

// Do not change
export let promise = writable();
export let receiveAmount = writable();
export let bestOption = writable([]);
export let hostName = writable("https://www.westernunion.com");
export let isOpen = writable(false);
export let isLoading = writable(false);